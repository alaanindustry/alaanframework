<?php 

namespace Controller;

use Config;
use Views;
use Mediator;
use Model;
use Requests;

Class HomeController{

	function __construct()
	{
		return Mediator\Login::isLoggedIn();
	}
	
	public function index()
	{	
		Views\App::view("home");
	}

	public function showlogin()
	{
		Views\App::view("Auth.login");
	}

	public function login(Requests\Request $request)
	{
		if (Mediator\Login::login($request)==null) {
			header("location: ".Config\App::url('/'));
		}else{
			Views\App::view("Auth.login");
		}
	}

	public function logout()
	{
		Mediator\Login::logout();
	}

	public function profile()
	{
		Model\User::setSettings();
		$user = Mediator\Auth::user();

		Views\App::view('Auth.profile',['user'=>$user]);
	}

	public function updateProfile(Requests\Request $request)
	{
		Model\User::setSettings();
		$validatevalues = [

			'firstname' => $request->firstname,
			'lastname' => $request->lastname,
			'username' => $request->username

		];

		$password = [

			'password' => $request->password,
			'confirm_password' => $request->confirm_password

		];

		$validator = new Mediator\Validator(['name','name','email'],$validatevalues);

    	if ($request->password!='') {

			$validator->newValidator(['password','confirm'],$password);
		}
		if ($validator->isValid()) {

			Model\User::set($validatevalues,Mediator\Auth::user()->id);

			if ($request->password!='') {
				Model\User::set([
					'password'=>Mediator\Security::ai_secure($request->password)
					],Mediator\Auth::user()->id);
			}

			header("Location: ".Config\App::url('/profile'));

		}else{
			return Views\App::view("Auth.profile",['user'=>$request]);
		}
	}

	public function auth()
	{
		Model\User::setSettings();
		return Mediator\Auth::user();
	}

}