<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class OrderController{

	
	function __construct()
	{
		return true;
	}

	public function index()
	{
		Model\Customer::setSettings();
		$products = Model\Product::query('*');
		return Views\App::view('Pages.Products.index',['products'=>$products]);
	}

	public function ordersCount()
	{
		Model\Order::setSettings();
		$orders = Model\Order::query('*');
		return count($orders);
	}

	public function payment()
	{

		if (Mediator\Auth::user()!='customer') {

			$customer = Mediator\Auth::user();

			return Views\App::view('Pages.Products.payment',['customer'=>$customer]);
		}else{
			return Views\App::view('Pages.Products.payment');
		}
	}

	public function validate(Requests\Request $request)
	{
		$validator = new Mediator\Validator(['name','name','email','phone','name'],$request);

        if (!$validator->isValid()) {
            return $validator->getErrors();
        }
	}

	public function store(Requests\Request $request)
	{
		if (isset(Mediator\Auth::user()->username)) {
				
			Model\Temporder::setSettings();
			$temporder = Model\Temporder::query('*');

			if (count($temporder)>0) {
				foreach ($temporder as $key => $value) {
					Model\Order::setSettings();
					$order = Model\Order::create([

						'customer_id' => Mediator\Auth::user()->id,
						'product_id' => $value['product_id'],
						'status' => 'done'


						]);

					Model\Product::setSettings();

					$product = Model\Product::query('*',['id',$value['product_id']]);

					Model\Product::set([

						'quantity' => $product[0]['quantity']-$value['quantity']

						],$value['product_id']);
				}
				Model\Temporder::setSettings();
				\Raw::query('delete from temporders');	
			}
		}else{

			Model\Temporder::setSettings();
			$temporder = Model\Temporder::query('*');

			if (count($temporder)>0) {

				Model\Customer::setSettings();

					$customer = Model\Customer::create([


						'firstname' => $request->firstname,
						'lastname' => $request->lastname,
						'email' => $request->email,
						'phone' => $request->phone,
						'address' => $request->address,


					]);

				foreach ($temporder as $key => $value) {
					Model\Order::setSettings();
					$order = Model\Order::create([

						'customer_id' => $customer,
						'product_id' => $value['product_id'],
						'status' => 'done'


						]);

					Model\Product::setSettings();	

					$product = Model\Product::query('*',['id',$value['product_id']]);
					
					Model\Product::set([

						'quantity' => $product[0]['quantity']-$value['quantity']

						],$value['product_id']);
				}

				Model\Temporder::setSettings();
				\Raw::query('delete from temporders');
			}

		}
		// $product = \Raw::query('select a.id,a.product_name,b.description as manufacturer,category_id,unit_id,unit_price,quantity,upc,sku,a.description,image,c.description as category,d.description as unit from products as a left join manufacturers as b on a.manufacturer_id = b.id left join categories as c on a.category_id = c.id left join units as d on a.unit_id = d.id where a.id = ?;',[$request->product_id]);

		// if (Mediator\Auth::user()=="guest") {

		// 	Model\Customer::setSettings();

		// 	$customer_id = "";
			
		// 	$customers = Model\Customer::query('*');

		// 	if (count($customers)==0) {
		// 		$customer = Model\Customer::create([

		// 			'firstname' => 'guest',
		// 			'lastname' => 'guest',


		// 		]);

		// 		$customer_id = $customer;

		// 	}else{
		// 		$customer_id = $customers[0]['id'];
		// 	}

		// 	Model\Order::setSettings();
		// 	$order = Model\Order::create([

		// 		'customer_id' => $customer_id,
		// 		'product_id' => $product[0]['id'],

		// 		]);
		// }else{

		// 	$cutomer_user_id = Mediator\Auth::user()->id;

		// 	Model\Order::setSettings();

		// 	$order = Model\Order::create([

		// 		'customer_id' => $cutomer_user_id,
		// 		'product_id' => $product['id'],

		// 		]);
		// }
	}
}