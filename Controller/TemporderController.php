<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class TemporderController{

	
	function __construct()
	{
		return true;
	}

	public function index()
	{
		return Views\App::view('Pages.Products.selected');
	}

	public function store(Requests\Request $request)
	{
		Model\Temporder::setSettings();

		Model\Temporder::create([

			'product_id' => $request->product_id

			]);
	}

	public function checkOut(Requests\Request $request)
	{
		$allselecteditems = $request->ob;

		foreach ($allselecteditems as $key => $value) {
			Model\Temporder::setSettings();
			Model\Temporder::set([

				'quantity' => $value['quantity'],
				'status' => 'processing'

				],$value['orderid']);
		}
	}

	public function getTempOrders()
	{
		$temporders = \Raw::query('select
									a.id as orderid,b.id,product_name,manufacturer_id,a.status,
									category_id,unit_id,unit_price,b.quantity as leftstock,a.quantity,upc,sku,b.description,
									c.description as manufacturer,d.description as category,image,e.description as unit
									from temporders as a left join products as b
									on a.product_id = b.id
									left join manufacturers as c on b.manufacturer_id = c.id
									left join categories as d on b.category_id = d.id
									left join units as e on b.unit_id = e.id;');
		return $temporders;
	}

	public function tempOrdersCount()
	{
		Model\Temporder::setSettings();
		$orders = Model\Temporder::query('*');
		return count($orders);
	}

}