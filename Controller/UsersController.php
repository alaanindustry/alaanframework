<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class UsersController{

    function __construct()
    {
        return Mediator\Login::isLoggedIn();
    }

    public function index(){

        return Views\App::view("Pages.Users.index");
    }

    public function create(){
    	return Views\App::view("Pages.Users.create");
    }

    public function store(Requests\Request $request)
    {
        
    	$validator = new Mediator\Validator(['name','name','email_exist','password','confirm'],$request);

    	if ($validator->isValid()) {

            Model\User::setSettings();
            $user = Model\User::create([
                'username'  => $request->username,
                'password'  => Mediator\Security::ai_secure($request->password),
                'activate'  => '1',
                'role' => 'admin'
            ]);

            Model\Admin::setSettings();
			$admin = Model\Admin::create([
			
			'firstname' => $request->firstname,
			'lastname'	=> $request->lastname,
            'user_id' => $user
			
			]);

            header("Location: " . \Config\App::url('/users/create'));

    	}else{
            return Views\App::view("Pages.Users.create");
        }
    }

    public function changeStatus($user_id)
    {
        Model\User::setSettings();
        $user = Model\User::query("*",["id",$user_id]);
        Model\User::set(['activate'=>($user[0]['activate'])=='1'?'0':'1'],$user_id);
    }

    public function editUser($user_id)
    {
        $user = \Raw::query('select a.id as user_id,b.id,role,username,password,token,activate,firstname,lastname from users as a left join admins as b on a.id = b.user_id where user_id = ?',[$user_id]);
        return $user[0];
    }

    public function validateUpdateUser(Requests\Request $request)
    {

        $validator = new Mediator\Validator(['name','name','email'],$request);

        if (!$validator->isValid()) {
            return $validator->getErrors();
        }
    }

    public function updateUser(Requests\Request $request)
    {

        Model\User::setSettings();
        Model\User::set([


            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'username' => $request->username


            ],$request->id);

    }

    public function deleteUser($user_id)
    {
        Model\User::setSettings();
        Model\User::delete($user_id);
    }

    public function validateUpdatePassword(Requests\Request $request)
    {
        $validator = new Mediator\Validator(['password','confirm'],$request);

        if (!$validator->isValid()) {
            return $validator->getErrors();
        }
    }

    public function updatePassword(Requests\Request $request)
    {
        Model\User::setSettings();
        Model\User::set([


            'password' => Mediator\Security::ai_secure($request->password),


            ],$request->id);
    }

    public function getUsers()
    {
        $users = \Raw::query('select a.id as user_id,b.id,role,username,password,token,activate,firstname,lastname from users as a left join admins as b on a.id = b.user_id where user_id != ?',[Mediator\Auth::user()->user_id]);
        
    	return $users;
    }
}

