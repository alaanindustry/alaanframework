<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class CustomerController{

	
	function __construct()
	{
		return true;
	}

	public function index()
	{
		Model\Customer::setSettings();
		
		$customers = Model\Customer::query('*');

		return Views\App::view('Pages.Customers.index',['customers'=>$customers]);
	}	

}