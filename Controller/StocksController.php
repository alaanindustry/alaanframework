<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class StocksController{
	
    function __construct()
    {
        Model\User::setSettings();
        if (Mediator\Auth::user()=="guest") {
            return true;
        }
        return Mediator\Login::isLoggedIn();
    }

    public function index(){

    	Model\Product::setSettings();
    	$products = Model\Product::query('*');

        return Views\App::view("Pages.Stocks.index",['products'=>$products]);
    }

    public function create()
    {
    	Model\Unit::setSettings();
    	$units = Model\Unit::query('*');
    	Model\Category::setSettings();
    	$categories = Model\Category::query('*');
    	Views\App::view("Pages.Stocks.create",['units'=>$units,'categories'=> $categories]);
    }

    public function store(Requests\Request $request)
    {
    	$validateValues = [

    		'name' => $request->name,
    		'manufacturer' => $request->manufacturer,
    		'price' => $request->price,
    		'quantity' => $request->quantity

    	];

    	$validator = new Mediator\Validator(['name','name','double','number'],$validateValues);

    	if ($validator->isValid()) {

    		$save_path = '/Assets/img/products/'.basename($request->files->image['name']);

    		$app_path = '/InventoryStuff/Assets/img/products/';

    		$target_dir = $_SERVER['DOCUMENT_ROOT'].$app_path;

			$target_file = $target_dir . basename($request->files->image['name']);

			$check = getimagesize($request->files->image['tmp_name']);

			$size = $request->files->image['size'];

			move_uploaded_file($request->files->image['tmp_name'], $target_file);
    		
			Model\Manufacturer::setSettings();

			$manufacturer =  Model\Manufacturer::query('*',['description',$request->manufacturer]);

			if (count($manufacturer)==0) {

				$manufacturer = Model\Manufacturer::create([

				'description' => $request->manufacturer

				]);

			}else{
				$manufacturer = $manufacturer[0]['id'];

			}
    		Model\Product::setSettings();
    		Model\Product::create([

    			'product_name' => $request->name,
    			'manufacturer_id' => $manufacturer,
    			'category_id' => $request->category,
    			'unit_id' => $request->unit,
    			'unit_price' => $request->price,
    			'quantity' => $request->quantity,
    			'upc' => $this->upc(),
    			'sku' => $this->sku($request->name,$manufacturer),
    			'description' => $request->description,
    			'image' => $save_path,

    			]);

    		header("Location: ". \Config\App::url('/stocks/create'));
    	}else{
    		Model\Unit::setSettings();
	    	$units = Model\Unit::query('*');
	    	Model\Category::setSettings();
	    	$categories = Model\Category::query('*');
    		return Views\App::view("Pages.Stocks.create",['units'=>$units,'categories'=> $categories]);
    	}
    }

    public function getStocks()
    {
    	Model\Product::setSettings();
    	$products = Model\Product::query('*');
    	return $products;
    }

    public function editStock($stock_id)
    {
    	$product = \Raw::query('select a.id,a.product_name,b.description as manufacturer,category_id,unit_id,unit_price,quantity,upc,sku,a.description,image,c.description as category,d.description as unit from products as a left join manufacturers as b on a.manufacturer_id = b.id left join categories as c on a.category_id = c.id left join units as d on a.unit_id = d.id where a.id = ?;',[$stock_id]);
        return $product[0];
    }

    public function updateStock(Requests\Request $request)
    {
    	$validator = new Mediator\Validator(['name','name','double'],[


    			'product_name' => $request->name,
    			'manufacturer' => $request->manufacturer,
    			'price' => $request->price


    		]);

    	if (!$validator->isValid()) {
    		return $validator->getErrors();
    	}else{
    		
    		if ($request->files->image['tmp_name']!="") {

    			$save_path = '/Assets/img/products/'.basename($request->files->image['name']);

	    		$app_path = '/InventoryStuff/Assets/img/products/';

	    		$target_dir = $_SERVER['DOCUMENT_ROOT'].$app_path;

				$target_file = $target_dir . basename($request->files->image['name']);

				$check = getimagesize($request->files->image['tmp_name']);

				$size = $request->files->image['size'];

				move_uploaded_file($request->files->image['tmp_name'], $target_file);

				Model\Product::setSettings();

				Model\Product::set([

    			'image' => $save_path,

    			],$request->id);
    		}

    		Model\Manufacturer::setSettings();
    	
			$manufacturer =  Model\Manufacturer::query('*',['description',$request->manufacturer]);

			if (count($manufacturer)==0) {

				

				$manufacturer = Model\Manufacturer::create([

				'description' => $request->manufacturer

				]);

			}else{

				$manufacturer = $manufacturer[0]['id'];

			}
    		Model\Product::setSettings();

    		Model\Product::set([


    			'product_name' => $request->name,
    			'manufacturer_id' => $manufacturer,
    			'category_id' => $request->category,
    			'unit_id' => $request->unit,
    			'unit_price' => $request->price,
    			'description' => $request->description,

    			],$request->id);

    	}
    }

    public function addQuantity($product_id,$quantity)
    {
    	Model\Product::setSettings();

    	$product = Model\Product::query('*',['id',$product_id]);

    	Model\Product::set([

    			'quantity' => $product[0]['quantity'] + $quantity

    		],$product_id);
    }

    public function setQuantity($product_id,$quantity)
    {
    	Model\Product::setSettings();

    	Model\Product::set([

    			'quantity' => $quantity

    		],$product_id);
    }

    public function upc() {

    	$result = '';

	    for($i = 0; $i < 12; $i++) {
	        $result .= mt_rand(0,9);
	    }

	    return $result;
	}

	public function sku($string, $id = null, $l = 2){

	    $results = '';
	    $vowels = array('a', 'e', 'i', 'o', 'u', 'y');
	    preg_match_all('/[A-Z][a-z]*/', ucfirst($string), $m);
	    foreach($m[0] as $substring){
	        $substring = str_replace($vowels, '', strtolower($substring));
	        $results .= preg_replace('/([a-z]{'.$l.'})(.*)/', '$1', $substring);
	    }
	    $results .= '-'. str_pad($id, 4, 0, STR_PAD_LEFT);
	    return $results;
	}
}

