<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class GuestController{

	
	function __construct()
	{
		return true;
	}

	public function index()
	{
		Model\Product::setSettings();
		$products = Model\Product::query('*');
		return Views\App::view('Pages.Products.index',['products'=>$products]);
	}	

	public function register()
	{
		return Views\App::view('Pages.Products.register');
	}

	public function store(Requests\Request $request)
	{
		$validator = new Mediator\Validator(['name','name','email','phone','name','password','confirm'],$request);

		if ($validator->isValid()) {

			Model\User::setSettings();
			$user = Model\User::create([

					'username' => $request->email,
					'password' => Mediator\Security::ai_secure($request->password),
					'role' => 'customer',
					'activate' => '1',

				]);

			Model\Customer::setSettings();
			Model\Customer::create([

					'firstname' => $request->firstname,
					'lastname' => $request->lastname,
					'email' => $request->email,
					'phone' => $request->phone,
					'address' => $request->address,
					'user_id' => $user

				]);

			header("Location: ".\Config\App::url('/login'));
		}else{
			return Views\App::view('Pages.Products.register');
		}
	}

}