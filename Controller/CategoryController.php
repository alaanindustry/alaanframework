<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class CategoryController{

	
	function __construct()
	{
		return Mediator\Login::isLoggedIn();
	}

	public function validate(Requests\Request $request)
	{
		$validator = new Mediator\Validator(['name'],$request);
		if (!$validator->isValid()) {
			return $validator->getErrors();
		}
	}	

	public function store(Requests\Request $request)
	{

		Model\Category::setSettings();

		Model\Category::create([

			'description' => $request->category


			]);
		
	}

	public function getCategories()
	{
		Model\Category::setSettings();
		$categories = Model\Category::query('*');
		return $categories;

	}



}