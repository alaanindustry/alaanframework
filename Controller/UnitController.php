<?php 

namespace Controller;

use Views;
use Mediator;
use Model;
use Requests;

Class UnitController{


	function __construct()
	{
		return Mediator\Login::isLoggedIn();
	}

	public function validate(Requests\Request $request)
	{
		$validator = new Mediator\Validator(['name'],$request);
		if (!$validator->isValid()) {
			return $validator->getErrors();
		}
	}	

	public function store(Requests\Request $request)
	{
		Model\Unit::setSettings();

		Model\Unit::create([

			'description' => $request->unit


			]);
	}
	
	public function getUnits()
	{
		Model\Unit::setSettings();
		$units = Model\Unit::query('*');

		return $units;
	}	

}