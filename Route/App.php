<?php

namespace Route;

use Route\Route;

Class App{

	function __construct() {

		/*
		|-------------------------------------------------------------------------------
		| Guest Route
		|-------------------------------------------------------------------------------
		*/

		Route::get('products','GuestController#index');

		Route::get('products/register','GuestController#register');

		Route::post('products/addnewcustomer','GuestController#store');

		/*
		|-------------------------------------------------------------------------------
		| Auth Route
		|-------------------------------------------------------------------------------
		*/
		Route::get('','HomeController#index');

		Route::get('login','HomeController#showlogin');

		Route::get('logout','HomeController#logout');

		Route::post('login','HomeController#login');

		Route::get('auth','HomeController#auth');

		Route::get('profile','HomeController#profile');

		Route::post('updateprofile','HomeController#updateProfile');

		/*
		|-------------------------------------------------------------------------------
		| Users Route
		|-------------------------------------------------------------------------------
		*/

		Route::get('users','UsersController#index');

		Route::post('users','UsersController#store');

		Route::get('users/create','UsersController#create');

		Route::get('users/getusers','UsersController#getUsers');

		Route::get('users/changestatus/{user_id}','UsersController#changeStatus');

		Route::get('users/edituser/{user_id}','UsersController#editUser');

		Route::post('users/validateupdateuser','UsersController#validateUpdateUser');

		Route::post('users/updateuser','UsersController#updateUser');

		Route::get('users/deleteuser/{user_id}','UsersController#deleteUser');

		Route::post('users/updatepassword','UsersController#updatePassword');

		Route::post('users/validateupdatepassword','UsersController#validateUpdatePassword');

		/*
		|-------------------------------------------------------------------------------
		| Stocks Route
		|-------------------------------------------------------------------------------
		*/

		Route::get('stocks','StocksController#index');

		Route::get('stocks/create','StocksController#create');

		Route::post('stocks','StocksController#store');

		Route::get('stocks/getstocks','StocksController#getStocks');

		Route::get('stocks/editstock/{stock_id}','StocksController#editStock');

		Route::post('stocks/updatestock','StocksController#updateStock');

		Route::get('stocks/addquantity/{id}/{quantity}','StocksController#addQuantity');

		Route::get('stocks/setquantity/{id}/{quantity}','StocksController#setQuantity');

		/*
		|-------------------------------------------------------------------------------
		| Category Route
		|-------------------------------------------------------------------------------
		*/

		Route::get('categories/getcategories','CategoryController#getCategories');

		Route::post('categories/validatecreatecategory','CategoryController#validate');

		Route::post('categories','CategoryController#store');



		/*
		|-------------------------------------------------------------------------------
		| Units Route
		|-------------------------------------------------------------------------------
		*/

		Route::get('units/getunits','UnitController#getUnits');
		
		Route::post('units/validatecreateunit','UnitController#validate');

		Route::post('units','UnitController#store');

		/*
		|-------------------------------------------------------------------------------
		| Orders Route
		|-------------------------------------------------------------------------------
		*/	
	
		Route::get('orders/getcustomerorders','OrderController#getOrders');

		Route::get('orders/getcustomerorderscount','OrderController#ordersCount');

		Route::post('orders/addorder','OrderController#store');

		Route::post('orders/validateaddorder','OrderController#validate');

		Route::get('orders/payment','OrderController#payment');


		/*
		|-------------------------------------------------------------------------------
		| Temp_Orders Route
		|-------------------------------------------------------------------------------
		*/	
		
		Route::get('temporders','TemporderController#index');
		
		Route::get('temporders/gettemporders','TemporderController#getTempOrders');

		Route::get('temporders/temporderscount','TemporderController#tempOrdersCount');

		Route::post('temporders/addtemporder','TemporderController#store');

		Route::post('temporders/checkout','TemporderController#checkOut');

		/*
		|-------------------------------------------------------------------------------
		| Customers Route
		|-------------------------------------------------------------------------------
		*/	
	
		Route::get('customers','CustomerController#index');

	}
}

