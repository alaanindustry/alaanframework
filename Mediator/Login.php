<?php 

namespace Mediator;

use Model;
use Mediator;
use Responses;
use Views;

Class Login{

	public function login($request)
	{
		Model\User::setSettings();
		$username = $request->username;
		$password = $request->password;
		$remember = isset($request->remember)?true:false;
		$user = Model\User::query("*");
		for ($i=0; $i < count($user); $i++) {
			$registered_password =\Mediator\Security::ai_show($user[$i]['password']);
			if ($registered_password==$password && $user[$i]['username']==$username && $user[$i]['activate']) {
				$token = "default";

				$token = Mediator\Security::ai_secure($user[$i]['id']);
				if ($remember) {

					Model\Model::set(['token'=>$token],$user[$i]['id']);
				}else{
					Model\Model::set(['token'=>'loggedin'],$user[$i]['id']);
				}
				setcookie("User", null, time()-3600);
				setcookie('User',$token);
				return null;
			}
		}

		$errors = ['username'=>'This credential is not defined'];
		$_POST['errors'] = $errors;
		return $errors;

	}

	public static function isLoggedIn()
	{
		Model\User::setSettings();
		if (isset($_COOKIE['User'])) {
			if ($_COOKIE['User']!="loggedin") {
				return true;
			}
			$users = Model\User::query("*");
			foreach ($users as $key => $value) {
				$token = $value['token'].'<br>';
				$cookie = $_COOKIE['User'].'<br>';
				if ($token==$cookie) {
					return true;
				}
			}
			return false;
		}else{
			$users = Model\User::query("*");
			foreach ($users as $key => $value) {
				if ($value['token']!="loggedout" && $value['token']!="" && ($value['token']!="loggedin")) {
					setcookie('User',$value['token']);
					return true;
				}
			}
			return false;
		}
	}

	public function logout()
	{	
		Model\User::setSettings();
		echo Mediator\Security::ai_show($_COOKIE['User']);
		Model\Model::set(['token'=>'loggedout'],Mediator\Security::ai_show($_COOKIE['User']));
		setcookie("User", null, time()-3600);
		header("Location:http://localhost/InventoryStuff/login");
	}
}