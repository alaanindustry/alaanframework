<?php

namespace Mediator;

use Model;

Class Security{

	public static $iv = "";

	public static function ai_secure($string)
	{
		// $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		// $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		// 
		$bytes = openssl_random_pseudo_bytes(8, $cstrong);
    	$iv   = bin2hex($bytes);
    	self::$iv = $iv;
		$enc_name = openssl_encrypt(
		    $string,
		    'AES-256-CBC',
		    Model\Model::$secretkey,
		    0,
		    $iv
		);
		return base64_encode(($enc_name.'.'.self::$iv));
	}

	public static function ai_show($string)
	{
		if ($string!=null) {
			$firstdecrypt = base64_decode($string);
			$split = explode(".", $firstdecrypt);
			$password = $split[0];
			$iv = $split[1];

			return $decryptedMessage = openssl_decrypt($password,'AES-256-CBC', Model\Model::$secretkey, 0,$iv);
		}else{
			$error = 'Always throw this error';
			echo $error;
		}

	}
}