<?php

namespace Mediator;

use Responses;

Class Validator{

	private $errors = null;

	function __construct($rules,$request) {

		$results = array();

		$values = array();

		foreach ($request as $key => $value) {
			$values[] = $value;
		}

		$rule = new Rule();

		foreach ($rules as $key => $value) {
			$results[]= $rule->$value($values[$key]);
		}

		$errors = array();
		$inc = 0;
		foreach ($request as $key => $value) {
			if (!is_object($value)) {
				if ($results[$inc]!==null) {
					$errors[$key] = $results[$inc];
				}
				$inc++;
			}
		}

		$this->errors = $errors;
		if (count($this->errors>0)) {
			$_POST['errors'] = $this->errors;
		}		
	}

	public function newValidator($rules,$request)
	{
		$results = array();

		$values = array();

		foreach ($request as $key => $value) {
			$values[] = $value;
		}

		$rule = new Rule();

		foreach ($rules as $key => $value) {
			$results[]= $rule->$value($values[$key]);
		}

		$inc = 0;
		foreach ($request as $key => $value) {
			if ($results[$inc]!==null) {
				$this->errors[$key] = $results[$inc];
			}
			$inc++;
		}

		if (count($this->errors>0)) {
			$_POST['errors'] = $this->errors;
		}
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function isValid()
	{
		return (count($this->errors)==0?true:false);
	}
}