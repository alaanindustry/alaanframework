<?php

namespace Mediator;

use Model;
use Mediator;

Class Auth{

	public static function user(){

		$user = \Raw::query('select a.id as user_id,b.id,role,username,password,token,activate,firstname,lastname from users as a join admins as b on a.id = b.user_id;');

		foreach ($user as $key => $value) {
			$id = $value['user_id'];
			if (isset($_COOKIE['User'])) {
				$cookie_id = Mediator\Security::ai_show($_COOKIE['User']);
				if ($id==$cookie_id) {
					$user = (Object) $value;
					return $user;
				}
			}
		}

		$user = \Raw::query('select b.id as user_id,a.id,firstname,lastname,username,phone,address,role,password,activate from customers as a join users as b on a.user_id = b.id;');	

		foreach ($user as $key => $value) {
			$id = $value['user_id'];
			if (isset($_COOKIE['User'])) {
				$cookie_id = Mediator\Security::ai_show($_COOKIE['User']);
				if ($id==$cookie_id) {
					$user = (Object) $value;
					return $user;
				}
			}
		}

		return 'guest';
	}
}