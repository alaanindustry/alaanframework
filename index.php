<?php

/*
|-------------------------------------------------------------------------------
| Author: Benjie Peria Alaan
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| Email: benjiealaan@gmail.com
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| Version: 1.1
|-------------------------------------------------------------------------------
*/

/*
|-------------------------------------------------------------------------------
| Welcome to Alaan Industry PHP Framework.
|-------------------------------------------------------------------------------
|
| This first version of framework is build to maximize the students projects.
| It was created for the student project purpose only. This version may contain
| some bugs and cannot guarranty a perfect project. If you want to 
| contribute please email me send some issues or bugs.
|
*/

/*
|-------------------------------------------------------------------------------
| File Register
|-------------------------------------------------------------------------------
|
| This file contain all of the the directories of the project
|
*/


/**
 * Controller
 */
require_once 'Controller/app.php';

/**
 * Databases
 */
require_once 'Database/config.php';
require_once 'Database/Connection.php';
require_once 'Database/Raw.php';
require_once 'Database/Query.php';

/**
 * Controllers
 */
require_once 'Controller/HomeController.php';
require_once 'Controller/UsersController.php';
require_once 'Controller/StocksController.php';
require_once 'Controller/CategoryController.php';
require_once 'Controller/UnitController.php';
require_once 'Controller/GuestController.php';
require_once 'Controller/OrderController.php';
require_once 'Controller/CustomerController.php';
require_once 'Controller/TemporderController.php';

/**
 * Models
 */

require_once 'Models/Model.php';
require_once 'Models/Unit.php';
require_once 'Models/User.php';
require_once 'Models/Category.php';
require_once 'Models/Product.php';
require_once 'Models/Manufacturer.php';
require_once 'Models/Order.php';
require_once 'Models/Customer.php';
require_once 'Models/Admin.php';
require_once 'Models/Temporder.php';

/**
 * Views
 */

require_once 'Views/App.php';

/**
 * Routes
 */

require_once 'Route/App.php';
require_once 'Route/Route.php';

/**
 * Mediator
 */

require_once 'Mediator/Login.php';
require_once 'Mediator/Security.php';
require_once 'Mediator/Auth.php';
require_once 'Mediator/Rule.php';
require_once 'Mediator/Validator.php';

/**
 * Responses
 */

require_once 'Responses/Response.php';
require_once 'Responses/Old.php';
require_once 'Responses/Error.php';

/**
 * Request
 */

require_once 'Requests/Request.php';
require_once 'Requests/File.php';

/**
 * Config
 */

require_once 'Config/App.php';

new Controller\App();
