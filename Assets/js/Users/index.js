new Vue({

	el:"#users",
	data:{
		users:[],
		user:[],
		updateUsererrors:[],
		updateNewPassword:{
			password:'',
			confirm_password:'',
			id:''
		},
		updatePasswordErrors:[]
	},
	ready:function(){
		this.getUsers();
	},
	methods:{
		toast:function (type,message) {
			Command: toastr[type](message)
			toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		},
		clickcreate:function(){
			window.location.replace(config.url('users/create'));
		},
		getUsers:function(){
			var vue = this;
			config.getData(this,config.url('users/getusers'), function(users){
				vue.users = users;
			});
		},
		changeStatus:function (user_id) {
			var vue = this;
			vue.$http.get(config.url('users/changestatus/'+user_id)).
			then((response) => {
				this.getUsers();
		    }, (response) => {
		    	console.warn(xhr.responseText);
		    });
		},
		updateUser:function (user_id) {
			var vue = this;
			vue.$http.get(config.url('users/edituser/'+user_id)).
			then((response)=>{
				$dataresponse = $.parseJSON(response.data);

				vue.user = $dataresponse;
				$('#edit-user').modal('show');
			});
		},
		clickValidateUpateUserForm:function () {
			var vue = this;
			$.ajax({
				url: config.url('users/validateupdateuser'),
				type: 'POST',
				data: {firstname:vue.user.firstname,lastname:vue.user.lastname,username:vue.user.username},
			})
			.done(function(response) {
				config.removeErrors(vue.updateUsererrors);
				try{
					$dataresponse = $.parseJSON(response);
					vue.updateUsererrors = $dataresponse;
					config.setErrors($dataresponse);
				}catch(err){
					$.ajax({
						url: config.url('users/updateuser'),
						type: 'POST',
						data: {firstname:vue.user.firstname,lastname:vue.user.lastname,username:vue.user.username,id:vue.user.id},
					}).done(function (response) {
						vue.updateUsererrors = [];
						vue.getUsers();
						$('#edit-user').modal('hide');
						vue.toast('info','Succesfully updated some changes');
					})
				}
			});
		},
		deleteUser:function (user_id) {
			var vue = this;
			if (confirm('Are you sure you wnat to totally delete this user')) {
				vue.$http.get(config.url('users/deleteuser/'+user_id)).
				then((response)=>{
					vue.getUsers();
					vue.toast('info','Succesfully deleted');
				});
			}else{
				alert('Canceled');
			}
		},
		editPassword:function (user_id) {
			var vue = this;
			vue.updateNewPassword.id = user_id;
			$('#edit-password').modal('show');
		},
		updatePassword:function () {
			var vue = this;
			var vue = this;
			$.ajax({
				url: config.url('/users/validateupdatepassword'),
				type: 'POST',
				data: {password:vue.updateNewPassword.password,confirm_password:vue.updateNewPassword.confirm_password},
			})
			.done(function(response) {
				config.removeErrors(vue.updatePasswordErrors);
				try{
					$dataresponse = $.parseJSON(response);
					vue.updatePasswordErrors = $dataresponse;
					config.setErrors($dataresponse);
				}catch(err){
					$.ajax({
						url: config.url('users/updatepassword'),
						type: 'POST',
						data: {password:vue.updateNewPassword.password,id:vue.updateNewPassword.id},
					}).done(function (response) {
						vue.getUsers();
						vue.updateNewPassword.password = '';
						vue.updateNewPassword.confirm_password = '';
						vue.updatePasswordErrors = [];
						$('#edit-password').modal('hide');
						vue.toast('info','Succesfully udpated password');
					})
				}
			});
		}
	}
})