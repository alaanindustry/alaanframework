Vue.validator('name',function (val){
	return /^(?! .[+*@#$%^&()])[a-zA-Z0-9\-\s]{2,}$/.test(val);
});

Vue.validator('email', function (val) {
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
});

new Vue({
	http: {
	    root: '/root',
	    headers: {
	      Authorization: 'Basic YXBpOnBhc3N3b3Jk'
	    }
	},
	el:'#createuser',
	data:{
		name:'',
		user:{
			firstname:'',
			lastname:'',
			email:'',
			password:'',
			confirm_password:''
		},
		error:{
			firstname:"",
			lastname:"",
			email:"",
			password:"",
			confirm_password:""
		}
	},
	validators: {
	    confirm: function (val, target) {
	      return val === target
	    }
	},
	methods:{
		clicksubmit:function(){
			var vue = this;
			$.ajax({
				url: config.url('users'),
				type: 'POST',
				data: {user:vue.user},
			})
			.done(function(response) {
				if (response!="") {
					var dataresponse = $.parseJSON(response);
					vue.error = dataresponse;
				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	}
});