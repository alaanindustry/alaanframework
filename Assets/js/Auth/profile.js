Vue.validator('name',function (val){
	return /^(?! .[+*@#$%^&()])[a-zA-Z0-9\-\s]{2,}$/.test(val);
});

Vue.validator('email', function (val) {
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
});
new Vue({

	el:'#updateprofile',
	data:{
		user:{
			firstname:'',
			lastname:'',
			email:'',
			password:'',
			confirm_password:''
		},
		error:{
			firstname:"",
			lastname:"",
			email:"",
			password:"",
			confirm_password:""
		}
	},
	ready:function(){
		var vue = this;
		$.ajax({
			url: config.url('auth'),
		})
		.done(function(response) {

			console.log("success");
			dataresponse = $.parseJSON(response);
			vue.user.firstname = dataresponse.firstname;
			vue.user.lastname = dataresponse.lastname;
			vue.user.email = dataresponse.username;
		});
	}
})