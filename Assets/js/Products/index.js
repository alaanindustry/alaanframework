new Vue({


	el:'body',
	data:{
		product:[],
		baseurl:'',
		count:0
	},
	ready:function (argument) {
		var vue = this;
		vue.baseurl = config.url('/');
		this.getTempOrdersCount();
		this.udpateSelected();
	},
	methods:{

		getTempOrdersCount:function(){
			var vue = this;
			config.orders(vue,function (count) {
				vue.count = count
			});
		},
		viewProduct:function (product_id) {
			var vue = this;
			vue.$http.get(config.url('stocks/editstock/'+product_id)).
			then((response)=>{
				console.log(response);
				$dataresponse = $.parseJSON(response.data);

				vue.product = $dataresponse;

				$('#view-product').modal('show');
			});
			
		},
		udpateSelected:function () {
			var vue = this;

			vue.$http.get(config.url('temporders/gettemporders')).
			then((response)=>{
				console.log(response.data);
				try{
					$dataresponse = $.parseJSON(response.data);
					$.each($dataresponse,function(index, value) {
						var p = $('.'+value.id).css("background", "-webkit-linear-gradient(45deg, #FFF, #a5e6b9)");
						$('.btn-'+value.id).hide();
					});
				}catch(err){

				}
				
			});
		},
		addOrder:function (product_id) {
			var vue = this;
			$.ajax({
				url: config.url('temporders/addtemporder'),
				type: 'POST',
				data: {product_id:product_id},
			})
			.done(function(response) {
				vue.getTempOrdersCount();
				vue.udpateSelected();
			});
			
		}
	}
})