new Vue({

	el:'body',
	data:{
		product:[],
		baseurl:'',
		count:0,
		temporders:[],
		setquantity:{},
		status:'',
		addOrderErrors:[]
	},
	ready:function (argument) {
		var vue = this;
		vue.baseurl = config.url('/');
		this.getTempOrdersCount();
		this.getTempOrders();
		
	},
	methods:{
		checkStatus:function (argument) {
			var vue = this;
			$.each(vue.temporders,function(index, el) {
				vue.status = el.status;
			});
		},
		getTempOrdersCount:function(){
			var vue = this;
			config.orders(vue,function (count) {
				vue.count = count
			});
		},
		getTempOrders:function(){
			var vue = this;
			config.getData(vue,config.url('/temporders/gettemporders'),function (temporders) {
				vue.temporders = temporders
				vue.checkStatus();
			});
		},
		checkOut:function () {

			
			var vue = this;
			var errors = 0;
			if (vue.status=='processing') {
				window.location.replace(config.url('/orders/payment'));
			}else{
				$.each(vue.temporders,function(index, value) {
					var quantity = $('[name="input-'+value.id+'"').val();
					$('[name="input-'+value.id+'"').css("border", "1px solid #333");
					$('#span-'+value.id).text("");
					if ((value.leftstock-quantity)>=0&&quantity!="") {
						value.quantity = quantity;
					}else{
						errors++;
						$('[name="input-'+value.id+'"').css("border", "1px solid rgb(187, 33, 33)");
						$('#span-'+value.id).text("Please check the quanity you entered");
					}
				});

				
				if (errors==0) {
					$.ajax({
						url: config.url('/temporders/checkout'),
						type: 'POST',
						data: {ob:vue.temporders},
					})
					.done(function(response) {
						window.location.replace(config.url('/orders/payment'));
					});
					
				}
			}
		},
		doneAll:function (customer_id) {

			var vue = this;
			var fromData = {};

			fromData.firstname = $('#firstname').val();

			fromData.lastname = $('#lastname').val();

			fromData.email = $('#email').val();

			fromData.phone = $('#phone').val();

			fromData.address = $('#address').val();

			$.ajax({
				url: config.url('orders/validateaddorder'),
				type: 'POST',
				data: fromData,
			})
			.done(function(response) {
				console.log(response);
				config.removeErrors(vue.addOrderErrors);
				try{
					$dataresponse = $.parseJSON(response);
					vue.addOrderErrors = $dataresponse;
					config.setErrors($dataresponse);
				}catch(err){
					$.ajax({
						url: config.url('orders/addorder'),
						type: 'POST',
						data: fromData,
					}).done(function (response) {
						vue.updateUsererrors = [];
						vue.getTempOrdersCount();
						config.toast('info','We\'ll contact your later.');
						window.location.replace(config.url('/products'));
					})
				}
			});
			
		}
	}


})