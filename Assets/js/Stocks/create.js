new Vue({
	el:'#create-stock',
	data:{
		categories:[],
		units:[],
		category:'',
		categoryError:'',
		unitError:'',
		unit:'',
	},
	ready:function () {
		this.getCategories();
		this.getUnits();
	},
	methods:{

		getCategories:function(){
			var vue = this;
			config.getData(this,config.url('/categories/getcategories'), function(categories){
				
				vue.categories = categories;
			});
		},
		getUnits:function(){
			var vue = this;
			config.getData(this,config.url('/units/getunits'),function(units){
				vue.units = units;
			});
		},
		newCategory:function() {
			$('#new-category').modal('show');
		},
		newUnit:function () {
			$('#new-unit').modal('show');
		},
		addCategory:function () {
			var vue = this;
			$.ajax({
				url: config.url('/categories/validatecreatecategory'),
				type: 'POST',
				data: {category:vue.category},
			})
			.done(function(response) {
				$('.category').removeClass('has-error');
				try{
					$dataresponse = $.parseJSON(response);
					vue.categoryError = $dataresponse;
					$('.category').addClass('has-error');
				}catch(err){
					$.ajax({
						url: config.url('/categories'),
						type: 'POST',
						data: {category:vue.category},
					}).done(function (response) {
						vue.getCategories();
						vue.category = '';
						vue.categoryError = '';
						$('#new-category').modal('hide');
						config.toast('success','Successfully add new category');
					})
				}
			})
		},
		addUnit:function () {
			var vue = this;
			$.ajax({
				url: config.url('/units/validatecreateunit'),
				type: 'POST',
				data: {unit:vue.unit},
			})
			.done(function(response) {
				$('.unit').removeClass('has-error');
				try{
					$dataresponse = $.parseJSON(response);
					vue.unitError = $dataresponse;
					$('.unit').addClass('has-error');
				}catch(err){
					$.ajax({
						url: config.url('/units'),
						type: 'POST',
						data: {unit:vue.unit},
					}).done(function (response) {
						vue.getUnits();
						vue.unit = '';
						vue.unitError = '';
						$('#new-unit').modal('hide');
						config.toast('success','Successfully add new unit');
					})
				}
			});
		},
	}

})