new Vue({
	el: '#stocks',
	data:{
		stocks:[],
		categories:[],
		stock:[],
		units:[],
		baseurl:'',
		quantity:'',
		product_id:''
	},
	ready:function(){
		this.getProducts();
		this.getCategories();
		this.getUnits();
		this.baseurl = config.url('/');
	},
	methods:{
		getCategories:function(){
			var vue = this;
			config.getData(this,config.url('/categories/getcategories'), function(categories){
				
				vue.categories = categories;
			});
		},
		getUnits:function(){
			var vue = this;
			config.getData(this,config.url('/units/getunits'),function(units){
				vue.units = units;
			});
		},
		getProducts:function(){
			var vue = this;
			config.getData(this,config.url('stocks/getstocks'), function(stocks){
				vue.stocks = stocks;

			});
		},
		editProduct:function (stock_id) {
			var vue = this;
			vue.$http.get(config.url('/stocks/editstock/'+stock_id)).
			then((response)=>{
				$dataresponse = $.parseJSON(response.data);
				vue.stock = $dataresponse;
				$('#edit-product').modal('show');
			});
		},
		updateProduct:function(){
			var vue = this;
			var formData = new FormData($('#update-product-form')[0]);
			$.ajax({
				url: config.url('stocks/updatestock'),
				type: 'POST',
				data: formData,
				contentType: false,
    			processData: false,
    			async: false,
    			cache: false,
			})
			.done(function (response) {
				config.toast('info',"Successfull updated!")
				$('#edit-product').modal('hide');
				vue.getProducts();
			})			
		},
		showQuantityModal:function (product_id) {
			var vue = this;
			vue.product_id = product_id;
			$('#quantity-modal').modal('show');
		},
		addQuantity:function () {
			var vue = this;
			vue.$http.get(config.url('/stocks/addquantity/'+vue.product_id+'/'+vue.quantity)).
			then((response)=>{
				vue.getProducts();
				$('#quantity-modal').modal('hide');
				console.log(response);
			});
		},
		setQuantity:function () {
			var vue = this;
			vue.$http.get(config.url('/stocks/setquantity/'+vue.product_id+'/'+vue.quantity)).
			then((response)=>{
				vue.getProducts();
				$('#quantity-modal').modal('hide');
				console.log(response);
			});
		}
	}	
});

// new Vue({
//   el: '#app',	
//   data: {
//     image: ''
//   },
//   methods: {
//     onFileChange(e) {
//       var files = e.target.files || e.dataTransfer.files;
//       if (!files.length)
//         return;
//       this.createImage(files[0]);
//     },
//     createImage(file) {
//       var image = new Image();
//       var reader = new FileReader();
//       var vm = this;

//       reader.onload = (e) => {
//         vm.image = e.target.result;
//       };
//       reader.readAsDataURL(file);
//     },
//     removeImage: function (e) {
//       this.image = '';
//     }
//   }
// })