<?php 


namespace Responses;

Class Error{


	function __construct($errors) {

		if ($errors!=null) {
			foreach ($errors as $key => $value) {
				$this->$key = $value;
			}
		}
		
	}

	public function has($param)
	{
		if (property_exists($this, $param)) {
			return true;
		}
		return false;
	}

}