<?php

namespace Config;

Class App{

	public static function url($url)
	{
		$configs = require 'conf.php';

		if ($url[0]=="/") {
			$url = substr($url, 1);
		}

		$newurl = $configs['baseUrl'].$url;

		return $newurl;

	}

}