<?php 

namespace Views;

use Responses;
	
Class App{


	public static function view($view,$ai = null)
	{
		
		$request = isset($_POST['old'])?$_POST['old']:null;
		$old = new Responses\Old($request);

		$error = isset($_POST['errors'])?$_POST['errors']:null;
		$errors = new Responses\Error($error);
		$ai = (Object) $ai;
		$chunkviewuri = explode(".", $view);

		$view = "";

		for ($i=0; $i < count($chunkviewuri); $i++) { 
			$view = $view.$chunkviewuri[$i]."/";
		}
		$view = rtrim($view,"/").".php";
		require_once $view;

	}
}