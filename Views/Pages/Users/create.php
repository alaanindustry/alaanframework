<?php require_once 'Views/Layout/app.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
	<div class="row" id="createuser">
		<div class="">
			<ol class="breadcrumb">
			<li><a href="<?php echo Config\App::url('/') ?>">Home</a></li>
			<li><a href="<?php echo Config\App::url('/users') ?>">Users</a></li>
			<li class="active">Create</li>
		</ol>
		<div class="panel panel-default">
			<div class="panel-heading">
				New User Form
			</div>
			<div class="panel-body">
				<form class="col-xs-8 col-xs-offset-2" method="POST" action="<?php echo Config\App::url('/users') ?>">
					<div class="form-group <?php echo $errors->has('firstname')?'has-error':''; ?>">
					    <label for="firstname">Firstname</label>
					    <input type="text" value="<?php $old->get('firstname'); ?>" name="firstname" class="form-control" id="firstname" placeholder="Enter Firstname">
	    				<?php if ($errors->has('firstname')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->firstname; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div class="form-group <?php echo $errors->has('lastname')?'has-error':''; ?>">
					    <label for="lastname">Lastname</label>
					    <input type="text" value="<?php $old->get('lastname'); ?>" name="lastname" class="form-control" id="lastname" placeholder="Enter Lastname">
	    				<?php if ($errors->has('lastname')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->lastname; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div class="form-group <?php echo $errors->has('username')?'has-error':''; ?>">
					    <label for="text">Email address</label>
					    <input type="text" value="<?php $old->get('username'); ?>" name="username" class="form-control" id="username" placeholder="Email">
	    				<?php if ($errors->has('username')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->username; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div class="form-group <?php echo $errors->has('password')?'has-error':''; ?>">
					    <label for="password">Password</label>
					    <input type="password" value="<?php $old->get('password'); ?>" name="password" class="form-control" id="password" placeholder="Enter Password">
					    <?php if ($errors->has('password')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->password; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div class="form-group <?php echo $errors->has('confirm_password')?'has-error':''; ?>">
					    <label for="comfirm_password">Confirm Password</label>
					    <input type="password" value="<?php $old->get('confirm_password'); ?>" name="confirm_password" class="form-control" id="confirm_password" placeholder="Enter Password">
	    				<?php if ($errors->has('confirm_password')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->confirm_password; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<button type="submit" class="btn btn-primary">
                        Submit
                    </button>
				</form>
			</div>
		</div>
		</div>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
    <script type="text/javascript" src="http://localhost/InventoryStuff/Assets/js/Users/create.js"></script>
    
</body>
</html>