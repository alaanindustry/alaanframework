<?php require_once 'Views/Layout/app.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
	<div class="row" id="users">
		<div class="all-modals">
			<div class="modal fade" id="edit-user" tabindex="-1" role="dialog">
			  	<div class="modal-dialog" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        		<h4 class="modal-title">User Details</h4>
			      		</div>
			      		<div class="modal-body">
			      			<form class="form-horizontal" id="form-edit-user" method="POST" action="<?php echo Config\App::url('/users/updateuser'); ?>">
							  	<div class="form-group firstname">
							    	<label for="inputEmail3" class="col-sm-3 control-label">Firstname</label>
							    	<div class="col-sm-9">
							    		<input type="hidden" v-model="user.id" name="id">
							      		<input type="text" v-model="user.firstname" name="firstname" class="form-control" id="inputEmail3" placeholder="Enter your firstname">
							      		<span v-if="updateUsererrors.firstname">
					    					<label class="control-label" for="inputError2">{{updateUsererrors.firstname}}</label>
					                    </span>
							    	</div>
							  	</div>
							  	<div class="form-group lastname">
							    	<label for="inputPassword3" class="col-sm-3 control-label">Lastname</label>
							    	<div class="col-sm-9">
							      		<input type="text" v-model="user.lastname" name="lastname" class="form-control" id="inputPassword3" placeholder="Enter your lastname">
							      		<span v-if="updateUsererrors.lastname">
					    					<label class="control-label" for="inputError2">{{updateUsererrors.lastname}}</label>
					                    </span>
							    	</div>
							  	</div>
							  	<div class="form-group username">
							    	<label for="inputPassword3" class="col-sm-3 control-label">Username</label>
							    	<div class="col-sm-9">
							      		<input type="text" v-model="user.username" name="username" class="form-control" id="username" placeholder="Enter your Email">
							      		<span v-if="updateUsererrors.username">
					    					<label class="control-label" for="inputError2">{{updateUsererrors.username}}</label>
					                    </span>
							    	</div>
							  	</div>
							</form>
			      		</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        		<button type="button" @click="clickValidateUpateUserForm()" class="btn btn-primary">Save changes</button>
			      		</div>
			    	</div>
			  	</div>
			</div>
	
			<div class="modal fade" tabindex="-1" id="edit-password" role="dialog">
			  	<div class="modal-dialog" role="document">
				    <div class="modal-content">
				      	<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Change Password</h4>
				      	</div>
				      	<div class="modal-body">
				        	<form class="form-horizontal">
				        		<div class="form-group password">
				        			<label class="col-sm-4 control-label">New Password</label>
				        			<div class="col-sm-8">
				        				<input type="password" v-model="updateNewPassword.password" name="password" class="form-control" id="password" placeholder="Enter your new Password">
							      		<span v-if="updatePasswordErrors.password">
					    					<label class="control-label" for="inputError2">{{updatePasswordErrors.password}}</label>
					                    </span>
				        			</div>
				        		</div>
				        		<div class="form-group confirm_password">
				        			<label class="col-sm-4 control-label">Confirm New Password</label>
				        			<div class="col-sm-8">
				        				<input type="password" v-model="updateNewPassword.confirm_password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Please confirm your new password">
							      		<span v-if="updatePasswordErrors.confirm_password">
					    					<label class="control-label" for="inputError2">{{updatePasswordErrors.confirm_password}}</label>
					                    </span>
				        			</div>
				        		</div>
				        	</form>
				      	</div>
				      	<div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="button" @click="updatePassword()" class="btn btn-primary">Save changes</button>
				      	</div>
				    </div><!-- /.modal-content -->
				 </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		</div>
		<ol class="breadcrumb">
			<li><a href="<?php echo Config\App::url('/') ?>">Home</a></li>
			<li class="active">Users</li>
		</ol>
		<div class="form-group">
		  	<button @click="clickcreate()" type="button" class="btn btn-primary">
		       	Add new User
		    </button>
		</div>
		<table class="table table-bordered" id="tableusers">
	  		<thead>
	  			<tr>
	  				<td></td>
	  				<td>Id</td>
	  				<td>Firstname</td>
	  				<td>Lastname</td>
	  				<td>Username</td>
	  				<td>Active</td>
	  			</tr>
	  		</thead>
	  		<tbody>
  				<tr v-for="user in users" v-cloak>
	  				<td style="">
	  					<div class="btn-group">
                          	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            	<i class="glyphicon glyphicon-align-justify"></i>
                            </button>
                          	<ul class="dropdown-menu pull-left" role="menu">
                            	<li><a href="#" @click="changeStatus(user.user_id)">{{user.activate=='1'?'Deactivate':'Activate'}}</a></li>
                            	<li><a href="#" @click="updateUser(user.user_id)">Update User</a></li>
                            	<li><a href="#" @click="deleteUser(user.user_id)">Delete</a></li>
                            	<li><a href="#" @click="editPassword(user.user_id)">Change Password</a></li>
                          	</ul>
                        </div>
	  				</td>
	  				<td>{{ user.user_id}}</td>
	  				<td>{{ user.firstname }}</td>
	  				<td>{{ user.lastname }}</td>
	  				<td>{{ user.username }}</td>
	  				<td>
  						<i v-if="user.activate==1" class="glyphicon glyphicon-ok"></i>
  						<i v-if="user.activate==0" class="glyphicon glyphicon-remove"></i>
	  				</td>
	  			</tr>
	  		</tbody>
	  	</table>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
    <script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/Users/index.js'); ?>"></script>
</body>
</html>