<?php require_once 'Views/Layout/app.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Stocks</title>
</head>
<body>
	<div class="row" id="stocks">
		<div class="all-modals">
			<div class="modal fade" id="quantity-modal" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Set Quantity</h4>
			      </div>
			      <div class="modal-body">
			        <form class="form-horizontal" @submit.prevent>
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-3 control-label">Quantity</label>
						    <div class="col-sm-7">
						      <input type="text" class="form-control" v-model="quantity" id="inputEmail3" placeholder="Please enter quantity">
						    </div>
						</div>
					</form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary" @click="addQuantity()">Add to Quanity</button>
			        <button type="button" class="btn btn-primary" @click="setQuantity()">Set to Quantity</button>
			      </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<div class="modal fade" tabindex="-1" id="edit-product" role="dialog">
			  	<div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Product Details</h4>
				      </div>
				      <div class="modal-body">
				        <form class="form-horizontal" id="update-product-form" enctype="multipart/form-data">
				        	<input type="hidden" name="id" value="{{stock.id}}">
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Product Image</label>
							    <div class="col-sm-6">
							      <img style="width:150px;height:150px" :src="baseurl+stock.image"  alt="" id="product-image-preview" class="img-thumbnail">
							    </div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label"></label>
							    <div class="col-sm-6">
							      <input type="file" name="image" id="product-image">
							    </div>
							</div>
							<div class="form-group product-name">
								<label for="inputEmail3" class="col-sm-4 control-label">Product Name</label>
								<div class="col-sm-6">
									<input type="text" v-model="stock.product_name" name="name" class="form-control" id="inputEmail3" placeholder="Please enter the new product name">
					                
								</div>
							</div>
					        <div class="form-group manufacturer">
					            <label for="inputEmail3" class="col-sm-4 control-label">Manufacturer Name</label>
					            <div class="col-sm-6">
					                <input type="text" v-model="stock.manufacturer" name="manufacturer" class="form-control" id="inputEmail3" placeholder="Please enter the manufacturer name">
					            </div>
					        </div>
					        <div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Category Name</label>
							    <div class="col-sm-6">
							      	<select name="category" v-model="stock.category_id" class="form-control">
					                    <option v-for="category in categories" value="{{category.id}}">{{category.description}}</option>
		  							</select>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Unit</label>
							    <div class="col-sm-6">
							    	<select name="unit" v-model="stock.unit_id" class="form-control">
					                    <option v-for="unit in units" value="{{unit.id}}">{{unit.description}}</option>
		  							</select>
							    </div>
							</div>
							<div class="form-group price">
							    <label for="inputEmail3" class="col-sm-4 control-label">Unit Price</label>
							    <div class="col-sm-6">
							      <input name="price" type="text" v-model="stock.unit_price" class="form-control" id="inputEmail3" placeholder="Please enter product unit price">
							    </div>
							</div>
					        <div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Description</label>
							    <div class="col-sm-6">
							      <textarea name="description" v-model="stock.description" class="form-control" id="inputEmail3" placeholder="You can fill in product description"></textarea>
							    </div>
							</div>
						</form>
				      </div>

				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updateProduct()">Save changes</button>
				      </div>
				    </div>
			  	</div>
			</div>
			<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  	<div class="modal-dialog">
				    <div class="modal-content">              
				      <div class="modal-body">
				      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <img src="" class="imagepreview" style="width: 100%;" >
				      </div>
				    </div>
			  	</div>
			</div>
		</div>
		<ol class="breadcrumb">
			<li><a href="http://localhost/InventoryStuff/">Home</a></li>
			<li class="active">Products</li>
		</ol>
		<div class="form-group">
		  	<a class="btn btn-primary" href="<?php echo Config\App::url('stocks\create'); ?>">Add New Product</a>
		</div>
		<table class="table table-responsive" id="tableusers">
	  		<thead>
	  			<tr>
	  				<th></th>
	  				<th>Product Name</th>
	  				<th>SKU</th>
	  				<th>UPC</th>
	  				<th>Price</th>
	  				<th>Quantity</th>
	  			</tr>
	  		</thead>
	  		<tbody>
  				<tr v-for="stock in stocks" v-cloak>
	  				<td>
	  					<a href="#" class="pop"><img style="width:50px;height:50px" :src="baseurl+stock.image"  alt="" id="product-image-preview" class="img-thumbnail"></a>
	  				</td>
	  				<td style="vertical-align:inherit"><a href="#" @click="editProduct(stock.id)">{{stock.product_name}}</a></td>
	  				<td style="vertical-align:inherit">{{stock.sku}}</td>
	  				<td style="vertical-align:inherit">{{stock.upc}}</td>
	  				<td style="vertical-align:inherit">{{stock.unit_price}}</td>
	  				<td style="vertical-align:inherit"><input v-on:dblclick="showQuantityModal(stock.id)" id="quantity" style="width:100px" type="text" readonly="" value="{{stock.quantity}}"></td>
	  			</tr>
	  		</tbody>
	  	</table>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
    <script type="text/javascript" src="http://localhost/InventoryStuff/Assets/js/Stocks/index.js"></script>
    <script type="text/javascript">

    $(function() {

    	$('#quantity-options').hide();
		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});
		// $( "#quantity" ).dblclick(function() {
		// 	$('#quantity-modal').modal('show');
		// });
	});
	(function() {
        var URL = window.URL || window.webkitURL;
        var input = document.querySelector('#product-image');
        var preview = document.querySelector('#product-image-preview');
        input.addEventListener('change', function () {
            preview.src = URL.createObjectURL(this.files[0]);
        });
        preview.addEventListener('load', function () {
            URL.revokeObjectURL(this.src);
        });

    })();
    </script>
</body>
</html>