<?php require_once 'Views/Layout/app.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Stocks</title>
</head>
<body>
	<div class="row" id="create-stock" v-cloak>
    <div class="all-modals">
      <div class="modal fade" id="new-category" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">New Category</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" @submit.prevent>
                <div class="form-group category">
                  <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" v-model="category" name="category" id="inputEmail3" placeholder="Please enter the name of the new Category">
                    <span v-if="categoryError.category">
                        <label class="control-label" for="inputError2">{{categoryError.category}}</label>
                    </span>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" @click="addCategory()" class="btn btn-primary">Save changes</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <div class="modal fade" id="new-unit" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">New Unit</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" @submit.prevent>
                <div class="form-group unit">
                  <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" v-model="unit" name="unit" id="inputEmail3" placeholder="Please enter the name of the new Unit">
                    <span v-if="unitError.unit">
                        <label class="control-label" for="inputError2">{{unitError.unit}}</label>
                    </span>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" @click="addUnit()" class="btn btn-primary">Save changes</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </div>
		<ol class="breadcrumb">
			<li><a href="http://localhost/InventoryStuff/">Home</a></li>
			<li><a href="http://localhost/InventoryStuff/stocks">Products</a></li>
			<li class="active">Add New Product</li>
		</ol>
		<div class="form-group">
		  	<a class="btn btn-primary" href="#" @click="newCategory()">Add New Category</a>
		  	<a class="btn btn-primary" href="#" @click="newUnit()">Add New Unit</a>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">New Product Form</div>
			<div class="panel-body">
				<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo Config\App::url('/stocks'); ?>">
					<div class="form-group">
					    <label for="inputEmail3" class="col-sm-3 control-label">Product Image</label>
					    <div class="col-sm-7">
					      <img style="width:150px;height:150px" src=""  alt="" id="product-image-preview" class="img-thumbnail">
					    </div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label"></label>
					    <div class="col-sm-7">
					      <input type="file" name="image" id="product-image">
					    </div>
					</div>
					<div class="form-group <?php echo $errors->has('name')?'has-error':''; ?>">
					    <label for="inputEmail3" class="col-sm-3 control-label">Product Name</label>
					    <div class="col-sm-7">
					      <input type="text" value="<?php $old->get('name'); ?>" name="name" class="form-control" id="inputEmail3" placeholder="Please enter the new product name">
                <?php if ($errors->has('name')): ?>
                  <span>
                    <label class="control-label" for="inputError2"><?php echo $errors->name; ?></label>
                          </span>
                <?php endif ?>
					    </div>
					</div>
          <div class="form-group <?php echo $errors->has('manufacturer')?'has-error':''; ?>">
              <label for="inputEmail3" class="col-sm-3 control-label">Manufacturer Name</label>
              <div class="col-sm-7">
                <input type="text" value="<?php $old->get('manufacturer'); ?>" name="manufacturer" class="form-control" id="inputEmail3" placeholder="Please enter the manufacturer name">
                <?php if ($errors->has('manufacturer')): ?>
                  <span>
                    <label class="control-label" for="inputError2"><?php echo $errors->manufacturer; ?></label>
                          </span>
                <?php endif ?>
              </div>
          </div>
					<div class="form-group">
					    <label for="inputEmail3" class="col-sm-3 control-label">Category Name</label>
					    <div class="col-sm-7">
					      	<select name="category" class="form-control">
                    <option v-for="category in categories" value="{{category.id}}">{{category.description}}</option>
  							  </select>
					    </div>
					</div>
					<div class="form-group">
					    <label for="inputEmail3" class="col-sm-3 control-label">Unit</label>
					    <div class="col-sm-7">
					    	<select name="unit" class="form-control">
							  	<option v-for="unit in units" value="{{unit.id}}">{{unit.description}}</option>
							  </select>  
					    </div>
					</div>
					<div class="form-group <?php echo $errors->has('price')?'has-error':''; ?>">
					    <label for="inputEmail3" class="col-sm-3 control-label">Unit Price</label>
					    <div class="col-sm-7">
					      <input name="price" type="text" value="<?php $old->get('price'); ?>" class="form-control" id="inputEmail3" placeholder="Please enter product unit price">
                <?php if ($errors->has('price')): ?>
                  <span>
                    <label class="control-label" for="inputError2"><?php echo $errors->price; ?></label>
                  </span>
                <?php endif ?>
					    </div>
					</div>
          <div class="form-group <?php echo $errors->has('quantity')?'has-error':''; ?>">
              <label for="inputEmail3" class="col-sm-3 control-label">Quantity</label>
              <div class="col-sm-7">
                <input name="quantity" type="text" value="<?php $old->get('quantity'); ?>" class="form-control" id="inputEmail3" placeholder="Please enter product quantity">
                <?php if ($errors->has('quantity')): ?>
                  <span>
                    <label class="control-label" for="inputError2"><?php echo $errors->quantity; ?></label>
                  </span>
                <?php endif ?>
              </div>
          </div>
					<div class="form-group">
					    <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
					    <div class="col-sm-7">
					      <textarea name="description" class="form-control" id="inputEmail3" placeholder="You can fill in product description"></textarea>
					    </div>
					</div>
					<div class="form-group">
					    <div class="col-sm-offset-3 col-sm-9">
					      <button type="submit" class="btn btn-default">Save new Product</button>
					    </div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  </footer>
  <?php include_once 'Views/Layout/footer.php'; ?>
  <script type="text/javascript" src="http://localhost/InventoryStuff/Assets/js/Stocks/create.js"></script>
  <script type="text/javascript">
    (function() {
        var URL = window.URL || window.webkitURL;
        var input = document.querySelector('#product-image');
        var preview = document.querySelector('#product-image-preview');
        input.addEventListener('change', function () {
            preview.src = URL.createObjectURL(this.files[0]);
        });
        preview.addEventListener('load', function () {
            URL.revokeObjectURL(this.src);
        });
    })();
  </script>
</body>
</html>