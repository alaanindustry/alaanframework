<?php require_once 'Views/Layout/app.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
	
	<div class="row">
		<form id="#checkout-form" @submit.prevent>
			<div class="list-group" >
			  <div class="list-group-item" style="border:none" v-for="temporder in temporders">

			  	<div style="text-align:center">
			  		<h4 class="list-group-item-heading">{{temporder.product_name}}</h4><span style="color:#63e0d0"> by : {{temporder.manufacturer}}, Price: {{temporder.unit_price}}, Category: {{temporder.category}}, Unit: {{temporder.unit}}</span>
			  		<p>Available Stock: {{temporder.leftstock }}</p>
				    <p class="list-group-item-text">{{temporder.description}}</p>
				    <img style="margin-top:14px;width:150px;height:150px" :src="baseurl+temporder.image"  alt="" id="product-image-preview" class="img-thumbnail">
				    <div><p>SKU: {{temporder.sku}}, UPC: {{temporder.upc}}</p></div>
				    <input v-if="temporder.status!='processing'" type="text" class="has-error" name="input-{{temporder.id}}" placeholder="Please enter quantity">
				    <div><span style="color:rgb(187, 33, 33)" id="span-{{temporder.id}}"></span></div>
				    <div v-if="status=='processing'"><p>Processing for Payment...</p></div>
			  	</div>
			    <hr>
			  </div>
			</div>
			<div style="text-align:center" class="form-group">
				<button v-if="count>0" class="btn btn-primary" @click="checkOut()"><i class="glyphicon glyphicon-ok"></i>{{status=='processing'?' Continue to payment':' Proceed to payment'}}</button>
			</div>
		</form>
		
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
  	<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/TempOrders/index.js'); ?>"></script>
</body>
</html>