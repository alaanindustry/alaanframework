<?php require_once 'Views/Layout/app.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#wrapper {
			width: 90%;
			max-width: 1100px;
			min-width: 700px;
			margin: 50px auto;
		}

		#columns {
			-webkit-column-count: 3;
			-webkit-column-gap: 10px;
			-webkit-column-fill: auto;
			-moz-column-count: 3;
			-moz-column-gap: 10px;
			-moz-column-fill: auto;
			column-count: 3;
			column-gap: 15px;
			column-fill: auto;
		}
		button{
			font-size: 12px;
		    font-weight: normal;
		    border: 1px solid #d0cdcd;
		    box-shadow: 1px 1px 1px #e0dfdf;
		    background-color: #fcfcfc;
		    margin-top: 17px;
		}

		.pin:hover{
			background: #F9F9F9;
		}

		.pin {
			display: inline-block;
			background: #FEFEFE;
			border: 1px solid #e4e2e2;
    		box-shadow: 0 1px 2px rgb(241, 238, 238);
			margin: 0 2px 15px;
			-webkit-column-break-inside: avoid;
			-moz-column-break-inside: avoid;
			column-break-inside: avoid;
			padding: 15px;
			padding-bottom: 5px;
			background: -webkit-linear-gradient(45deg, #FFF, #F9F9F9);
			opacity: 1;
			
			-webkit-transition: all .2s ease;
			-moz-transition: all .2s ease;
			-o-transition: all .2s ease;
			transition: all .2s ease;
		}

		.pin img {
			width: 100%;
			border-bottom: 1px solid #ccc;
			padding-bottom: 15px;
			margin-bottom: 5px;
		}

		.pin p {
			font: 12px/18px Arial, sans-serif;
			color: #333;
			margin: 0;
		}

		@media (min-width: 960px) {
			#columns {
				-webkit-column-count: 4;
				-moz-column-count: 4;
				column-count: 4;
			}
		}

		@media (min-width: 1500px) {
			#columns {
				-webkit-column-count: 5;
				-moz-column-count: 5;
				column-count: 5;
			}
		}

		#description{
			margin-bottom: 20px;
		}
	</style>
</head>
<body>
	<div class="row" id="all-products">
		<div class="all-modal">
			<div class="modal fade" id="view-product" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Product Details</h4>
			      </div>
			      <div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-4 control-label">Product Image</label>
						    <div class="col-sm-7">
						      <img style="width:150px;height:150px" :src="baseurl+product.image"  alt="" id="product-image-preview" class="img-thumbnail">
						    </div>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-4 control-label">Product Name</label>
						    <div class="col-sm-7">
						      	<p class="form-control-static">{{product.product_name}}</p>
						    </div>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-4 control-label">Manufacturer</label>
						    <div class="col-sm-7">
						      	<p class="form-control-static">{{product.manufacturer}}</p>
						    </div>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-4 control-label">Price</label>
						    <div class="col-sm-7">
						      	<p class="form-control-static">{{product.unit_price}}</p>
						    </div>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-4 control-label">Available Stocks</label>
						    <div class="col-sm-7">
						      	<p class="form-control-static">{{product.quantity}}</p>
						    </div>
						</div>
					</form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" @click="addOrder(product.id)" class="btn btn-primary {{'btn-'+product.id}}"><li class="glyphicon glyphicon-shopping-cart"></li>Add to Cart</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<div id="wrapper">
			<div id="columns">
				<?php foreach ($ai->products as $key => $value): ?>
					<?php if ($value['quantity']>0): ?>
						<div class="pin <?php echo $value['id']; ?>">
							<img src="<?php echo Config\App::url('/').$value['image']; ?>" alt="..." />
							<p style="margin-bottom:10px;font-weight:bold"><?php echo $value['product_name']; ?></p>
							<p id="description"><?php echo $value['description']; ?></p>
							<p>Stocks:<span> <?php echo $value['quantity']; ?></span></p>
							<p>Price:<span> ₱ <?php echo $value['unit_price']; ?></span></p>
							<button class="btn-<?php echo $value['id']; ?>" style="font-size:12px" @click="addOrder(<?php echo $value['id']; ?>)"><li class="glyphicon glyphicon-shopping-cart" ></li> Add to Cart</button>
							<button class="btn-<?php echo $value['id']; ?>" style="font-size:12px" @click="viewProduct(<?php echo $value['id']; ?>)"><li class="glyphicon glyphicon-eye-open"></li></button>
						</div>
					<?php endif ?>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
  	<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/Products/index.js'); ?>"></script>
</body>
</html>