<?php require_once 'Views/Layout/app.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
	
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-heading">Registration Form</div>
			<div class="panel-body">
				<form class="form-horizontal" method="POST" action="<?php echo Config\App::url('products/addnewcustomer'); ?>">
				  <div class="form-group <?php echo $errors->has('firstname')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Firstname</label>
				    <div class="col-sm-7">
				      <input type="text" class="form-control" value="<?php $old->get('firstname'); ?>" name="firstname" id="inputEmail3" placeholder="Please enter your name">
				      	<?php if ($errors->has('firstname')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->firstname; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div>
				  <div class="form-group <?php echo $errors->has('lastname')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Lastname</label>
				    <div class="col-sm-7">
				      <input type="text" value="<?php $old->get('lastname'); ?>" class="form-control" name="lastname" id="inputEmail3" placeholder="Please enter your lastname">
				      <?php if ($errors->has('lastname')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->lastname; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div>
				  <div class="form-group <?php echo $errors->has('email')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
				    <div class="col-sm-7">
				      <input type="text" value="<?php $old->get('email'); ?>" class="form-control" name="email" id="inputEmail3" placeholder="Please enter your email">
				      <?php if ($errors->has('email')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->email; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div>
				  <div class="form-group <?php echo $errors->has('phone')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Phone No.</label>
				    <div class="col-sm-7">
				      <input type="text" value="<?php $old->get('phone'); ?>" class="form-control" name="phone" id="inputEmail3" placeholder="Please enter you phone no">
				      <?php if ($errors->has('phone')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->phone; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div>
				  <div class="form-group <?php echo $errors->has('address')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
				    <div class="col-sm-7">
				      <input type="text" value="<?php $old->get('address'); ?>" class="form-control" name="address" id="inputEmail3" placeholder="Please enter you address">
				      <?php if ($errors->has('address')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->address; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div> 
				  <div class="form-group <?php echo $errors->has('password')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
				    <div class="col-sm-7">
				      <input type="password" value="<?php $old->get('password'); ?>" class="form-control" name="password" id="inputEmail3" placeholder="Please enter you password">
				      <?php if ($errors->has('password')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->password; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div>
				  <div class="form-group <?php echo $errors->has('confirm_password')?'has-error':''; ?>">
				    <label for="inputEmail3" class="col-sm-3 control-label">Confirm Password</label>
				    <div class="col-sm-7">
				      <input type="password" value="<?php $old->get('confirm_password'); ?>" class="form-control" name="confirm_password" id="inputEmail3" placeholder="Please re-enter you password">
				      <?php if ($errors->has('confirm_password')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->confirm_password; ?></label>
		                    </span>
	    				<?php endif ?>
				    </div>
				  </div> 
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-3 control-label"></label>
				    <div class="col-sm-7">
				      <button type="submit" class="btn btn-primary">Register</button>
				    </div>
				  </div>
				</form>
			</div>
		</div>
		
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
  	<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/TempOrders/index.js'); ?>"></script>
</body>
</html>