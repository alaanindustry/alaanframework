<?php require_once 'Views/Layout/app.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
	
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-heading">Delivery Information</div>
			<div class="panel-body">
				<form class="form-horizontal" @submit.prevent>
				  <div class="form-group">
				    <label for="firstname" class="col-sm-3 control-label">Firstname</label>
				    <div class="col-sm-7">

				      <input type="text" <?php echo (isset($ai->customer->firstname)?'readonly':''); ?> value="<?php echo (isset($ai->customer->firstname)?$ai->customer->firstname:''); ?>" class="form-control" id="firstname" placeholder="Please enter your name">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="lastname" class="col-sm-3 control-label">Lastname</label>
				    <div class="col-sm-7">
				      <input type="text" <?php echo (isset($ai->customer->firstname)?'readonly':''); ?> value="<?php echo (isset($ai->customer->lastname)?$ai->customer->lastname:''); ?>" class="form-control" id="lastname" placeholder="Please enter your lastname">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="email" class="col-sm-3 control-label">Email</label>
				    <div class="col-sm-7">
				      <input type="text" <?php echo (isset($ai->customer->firstname)?'readonly':''); ?> value="<?php echo (isset($ai->customer->username)?$ai->customer->username:''); ?>" class="form-control" id="email" placeholder="Please enter your email">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="phone" class="col-sm-3 control-label">Phone No.</label>
				    <div class="col-sm-7">
				      <input type="text" <?php echo (isset($ai->customer->firstname)?'readonly':''); ?> value="<?php echo (isset($ai->customer->phone)?$ai->customer->phone:''); ?>" class="form-control" id="phone" placeholder="Please enter you phone no">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="address" class="col-sm-3 control-label">Address</label>
				    <div class="col-sm-7">
				      <input type="text" <?php echo (isset($ai->customer->firstname)?'readonly':''); ?> value="<?php echo (isset($ai->customer->address)?$ai->customer->address:''); ?>" class="form-control" id="address" placeholder="Please enter you address">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-3 control-label"></label>
				    <div class="col-sm-7">
				      <button class="btn btn-primary" @click="doneAll(<?php echo (isset($ai->customer->firstname)?$ai->customer->id:''); ?>)">Done</button>
				      <?php if (!isset(Mediator\Auth::user()->role)): ?>
				      	<a href="<?php echo Config\App::url('/products/register'); ?>" class="btn btn-link">Dont have an account?</button>
				      <?php endif ?>
				    </div>
				  </div>
				</form>
			</div>
		</div>
		
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
  	<script type="text/javascript" src="<?php echo Config\App::url('/Assets/js/TempOrders/index.js'); ?>"></script>
</body>
</html>