<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Narrow Jumbotron Template for Bootstrap</title>
    <link href="http://localhost/InventoryStuff/Assets/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/InventoryStuff/Assets/css/jumbotron-narrow.css" rel="stylesheet">

  </head>

  <body>
    <div class="container">
      <div class="row">
      	<div class="col-xs-offset-2" style="margin-top:200px;color:#a9a9ab">
      		<h1 style="font-size:50px">Sorry! Can't find this page</h1>
      	</div>
      </div>
    </div>
  </body>
</html>