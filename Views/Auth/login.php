<?php require_once 'Views/Layout/app.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
  </head>

    <div class="container" id="logincontainer">
	    <div class="container col-xs-8 col-xs-offset-2 col-sm-offset-1 col-md-offset-1" style="margin-top:50px">
	    	<form class="form-horizontal" role="form" method="POST" action="http://localhost/InventoryStuff/login">
				
	    		<div class="form-group <?php echo $errors->has('username')?'has-error':''; ?>" id="div-id">
	    			<label for="username" class="col-sm-4 control-label">Username</label> 
	    			<div class="col-sm-8 ">
	    				<input type="text" value="<?php $old->get('username'); ?>" name="username" class="form-control" id="username" placeholder="Please enter your email">
	    				<?php if ($errors->has('username')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->username; ?></label>
		                    </span>
	    				<?php endif ?>
	    			</div>
	    		</div>
	    		<div class="form-group"> 
	    			<label for="password" class="col-sm-4 control-label">Password</label> 
	    			<div class="col-sm-8"> 
	    				<input type="password" v-model="password" name="password" class="form-control" id="password" placeholder="Please enter your password"> 
	    			</div> 
	    		</div> 
	    		<div class="form-group"> 
	    			<div class="col-sm-offset-4 col-sm-10"> 
	    				<div class="checkbox"> 
	    					<label> 
	    						<input type="checkbox" v-model="remember" name="remember"> Remember me 
	    					</label> 
	    				</div> 
	    			</div> 
	    		</div> 
	    		<div class="form-group"> 
	    			<div class="col-sm-offset-4 col-sm-10"> 
	    				<button type="submit" class="btn btn-default">Sign in</button> 
	    			</div> 
	    		</div> 
	    	</form>
	    </div>
    </div>
    <?php include_once 'Views/Layout/footer.php'; ?>
    <script type="text/javascript" src="http://localhost/InventoryStuff/Assets/js/Login/login.js"></script>

</html>