<?php require_once 'Views/Layout/app.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
	<div class="container" id="updateprofile">
		<div class="row">
			<ol class="breadcrumb">
			<li><a href="http://localhost/InventoryStuff/">Home</a></li>
			<li class="active">Profile</li>
		</ol>
		<div class="panel panel-default">
			<div class="panel-heading">
				Account Details
			</div>
			<div class="panel-body">
				<form class="col-xs-8 col-xs-offset-2" method="POST" action="http://localhost/InventoryStuff/updateprofile">
					<div class="form-group <?php echo $errors->has('firstname')?'has-error':''; ?>">
					    <label for="firstname">Firstname</label>
					    <input type="text" name="firstname" value="<?php echo $ai->user->firstname; ?>" class="form-control" id="firstname" placeholder="Enter Firstname">

	    				<?php if ($errors->has('firstname')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->firstname; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div class="form-group <?php echo $errors->has('lastname')?'has-error':''; ?>">
					    <label for="lastname">Lastname</label>
					    <input type="text" name="lastname" value="<?php echo $ai->user->lastname; ?>" class="form-control" id="lastname" placeholder="Enter Lastname">
	    				<?php if ($errors->has('lastname')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->lastname; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div class="form-group <?php echo $errors->has('username')?'has-error':''; ?>">
					    <label for="text">Email address</label>
					    <input type="text" name="username" value="<?php echo $ai->user->username; ?>" class="form-control" id="username" placeholder="Email">
	    				<?php if ($errors->has('username')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->username; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<div v-validate-class class="form-group password">
					    <label for="password">New Password</label>
					    <input type="password" name="password" value="<?php $old->get('password'); ?>" class="form-control" id="password" placeholder="Enter Password">
					    
	    				<span v-if="error.password" ai-error v-cloak>
	    					<label class="control-label" for="inputError2">{{ error.password }}</label>
	                    </span>
					</div>
					<div v-validate-class class="form-group confirm_password <?php echo $errors->has('confirm_password')?'has-error':''; ?>">
					    <label for="comfirm_password">Confirm New Password</label>
					    <input type="password"  name="confirm_password" value="<?php $old->get('confirm_password'); ?>" class="form-control" id="confirm_password"  placeholder="Enter Password">
	    				<?php if ($errors->has('confirm_password')): ?>
	    					<span>
		    					<label class="control-label" for="inputError2"><?php echo $errors->confirm_password; ?></label>
		                    </span>
	    				<?php endif ?>
					</div>
					<button type="submit" class="btn btn-default">
	                    Save Changes
	                </button>
	                <button class="btn btn-info">Discard All Changes</button>
				</form>
			</div>
		</div>
		</div>
	</div>
	<footer class="footer col-xs-12">
    	<p>&copy; 2016 AI Solutions Company, Inc.</p>
  	</footer>
  	<?php include_once 'Views/Layout/footer.php'; ?>
    <script type="text/javascript" src="http://localhost/InventoryStuff/Assets/js/Auth/profile.js"></script>
    
</body>
</html>