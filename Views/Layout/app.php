<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Narrow Jumbotron Template for Bootstrap</title>
    <link href="<?php echo Config\App::url('/Assets/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo Config\App::url('Assets/css/jumbotron-narrow.css'); ?>" rel="stylesheet">
    <link href="<?php echo Config\App::url('/Assets/css/toaster.css'); ?>" rel="stylesheet" type="text/css" >

  </head>

  <body>
    <div class="container">
      <div class="header clearfix">

        <?php if (isset(Mediator\Auth::user()->role)): ?>
          <?php if (Mediator\Auth::user()->role=="admin"): ?>
            <nav>
              <ul class="nav nav-pills pull-right">
                <li><a href="<?php echo Config\App::url('/users'); ?>">Users</a></li>
                <li><a href="<?php echo Config\App::url('/stocks'); ?>">Products</a></li>
                <li><a href="<?php echo Config\App::url('/customers'); ?>">Customer</a></li>
                <li><a href="<?php echo Config\App::url('/stocks'); ?>">Sales</a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-user"></i> </a>
                  <ul class="dropdown-menu">
                    <li style="background-color:#d0efee"><a><?php echo Mediator\Auth::user()->username; ?> <i class="glyphicon glyphicon-ok"></i></a> </li>
                    <li><a href="<?php echo Config\App::url('/profile'); ?>">Account</a></li>
                    <li><a href="<?php echo Config\App::url('/logout'); ?>">Logout</a></li>
                  </ul>
              </li>
              </ul>
            </nav>
          <?php else: ?>
            <nav> 
              <ul class="nav nav-pills pull-right">
                <li><a href="<?php echo Config\App::url('/products'); ?>"><i class="glyphicon glyphicon-gift"></i> AI Products</a></li>
                <li><a href="<?php echo Config\App::url('/temporders'); ?>"><span  style="background-color:#ea6868" class="badge">{{count}}</span><i class="glyphicon glyphicon-shopping-cart"></i> My Cart</a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-user"></i> </a>
                  <ul class="dropdown-menu">
                    <li><a><?php echo Mediator\Auth::user()->username; ?> <i class="glyphicon glyphicon-ok"></i></a> </li>
                    <li><a href="<?php echo Config\App::url('/logout'); ?>">Logout</a></li>
                  </ul>
              </li>
              </ul>
            </nav>
          <?php endif ?>
        <?php else: ?>
          <nav> 
            <ul class="nav nav-pills pull-right">
              <li><a href="<?php echo Config\App::url('/products'); ?>"><i class="glyphicon glyphicon-gift"></i> AI Products</a></li>
              <li><a href="<?php echo Config\App::url('/temporders'); ?>"><span  style="background-color:#ea6868" class="badge">{{count}}</span><i class="glyphicon glyphicon-shopping-cart"></i> My Cart</a></li>
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-user"></i> </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo Config\App::url('/login'); ?>">Sign In</a></li>
                  <li><a href="<?php echo Config\App::url('/products/register'); ?>">Register</a></li>
                </ul>
            </li>
            </ul>
          </nav>
        <?php endif ?>
        <h3 class="text-muted">Alaan Industry</h3>
      </div>
      <!-- </div> -->
  </body>
</html>