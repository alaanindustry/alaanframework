<?php


Class Raw extends Connection{


	public static function query($query,$params = null)
	{
		$connection = self::getConnection();
		$sth = $connection->prepare($query);
		$sth->execute($params);
		$yellow = $sth->fetchAll();

		return $yellow;
	}

}
