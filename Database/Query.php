<?php


Class Query{

	private $dataset;

	function __construct($dataset) {
		$this->dataset = $dataset;
	}

	public function get()
	{
		return $this->dataset;
	}

	public function first()
	{
		return $this->dataset[0];
	}


}